/**
 * Created with JetBrains WebStorm.
 * User: chris
 * Date: 26.03.13
 * Time: 08:46
 * To change this template use File | Settings | File Templates.
 */


// Init stuff
var DB_NAME = 'websql://ioforms',
    DB_NAME_FORMVALUES = 'websql://ioformvalues',
    currentFormDefID = "",
//var REMOTE_DB = 'http://127.0.0.1:2020/glasgowjs';


// Create a database
localFormDefinitionDB = Pouch(DB_NAME),
localFormValues = Pouch(DB_NAME_FORMVALUES),
formdefdata = [
    {
        type:"h1",
        html:"iOWeb Form from PouchDB"
    },
    {
        "type":"p",
        "html":"how are u today?"
    },
    {
        "name":"dateformentered",
        "id":"dateformentered",
        "caption":"Date",
        "type":"text",
        "placeholder":"01.02.2013"
    },
    {
        "name":"name",
        "caption":"name",
        "type":"text"
    },
    {
        "name":"hint",
        "caption":"hint",
        "type":"text"
    },
    {
        "type":"submit",
        "value":"Save Form",
        "onclick": "saveFormValue()"
    }
];


//Pouch.destroy(DB_NAME);
//Pouch.destroy(DB_NAME_FORMVALUES);

function updateFormDefinition(){
    // add anew entry, or update an existing
    localFormDefinitionDB.post({
        _id: "form1",
        data: formdefdata
    }, function (err, resp) {
        if(err){
            localFormDefinitionDB.put({ _id: "form1",
                data: formdefdata});
            console.log("updated formdefinition");
        }
    });
}


function renderForm(formid){
    updateFormDefinition();
    localFormDefinitionDB.get(formid, function (err, form) {
        if (err) {
            alert("unable to get()");
        }else{
            $("#myform").dform({
                "action":"",
                "method":"",
                "data-in": formid,
                "html":form.data
            });
        }
    });

}

function saveFormValue(){
    var formdata =  jQuery('#myform > form').serializeObject() ,
        formdatavalues = "[".concat(JSON.stringify(formdata),"]"),
        formdefidval = jQuery('#myform > form').attr("data-in"),
        _postid;

    console.log( "\nValues serialized before post: " );
    console.log( formdatavalues );
    localFormValues.post( {
            formdefid: formdefidval,
            formvalues: formdatavalues
        }, function (err, resp) {
            if(err){
                alert("Unable to post values to formvaluedb: " + formdatavalues);
            }else{
                _postid = resp._id;
                console.log("!!!saved values!!!! " + _postid );
            }
        }

    );
}

function logDBS(){

// List the stuff in the formdefinition database
    localFormDefinitionDB.allDocs({include_docs:true}, function (err, docs) {
        if (err) {
            alert("ERROR logging allDocs" + err);
        }

        console.log("ALL DOCS: " );
        console.log( docs.rows);
    });


// List the stuff in the form value database
    localFormValues.allDocs({include_docs:true}, function (err, docs) {
        if (err) {
            alert("ERROR logging allDocs" + err);
        }

        console.log("ALL formvalue documents: " );
        console.log( docs.rows);
    });

}


